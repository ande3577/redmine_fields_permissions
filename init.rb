require 'redmine'

Redmine::Plugin.register :redmine_fields_permissions do
  name 'Redmine Fields Permissions Plugin'
  author 'David Anderson'
  description 'This Redmine plugin add additional permissions for fields in workflow. This plugin is based on http://9thport.net/2011/03/20/redmine-hide-assigned-to-field-with-role-permissions-plugin/ by Aaron Addleman'
  version '1.0.0'
  url 'https://bitbucket.org/ande3577/redmine_fields_permissions/overview'
  author_url 'https://bitbucket.org/ande3577/redmine_fields_permissions/overview'

  project_module :issue_tracking do
    
    # Permissions for assigned to field
    permission :edit_assigned_to, :issues => :index
    permission :view_assigned_to, :issues => :index
    
    #Permissions for start date field
    permission :edit_start_date, :issues => :index
    permission :view_start_date, :issues => :index
    
    # Permissions for due date field
    permission :edit_due_date, :issues => :index
    permission :view_due_date, :issues => :index
    
    # Permissions for estimated hours field
    permission :edit_estimated_hours, :issues => :index
    permission :view_estimated_hours, :issues => :index
  
    # Permissions for priority field
    permission :edit_priority, :issues => :index
    permission :view_priority, :issues => :index
  
    # Permissions for fixed version field
    permission :edit_fixed_version, :issues => :index
    permission :view_fixed_version, :issues => :index
    
    # Permissions for issue category field
    permission :edit_issue_category, :issues => :index
    permission :view_issue_category, :issues => :index
    
    #Permission for editing the parent task
    permission :edit_parent_task, :issues => :index
    permission :view_parent_task, :issues => :index
    
    # Permissions for custom fields
    permission :edit_custom_field, :issues => :index
    permission :view_custom_field, :issues => :index
    
    # Permission for editing a users own issue
    permission :edit_own_issue, :issues => :index
    
  end
  
  
end

